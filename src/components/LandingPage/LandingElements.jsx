import { Link } from "react-router-dom";
import styled from "styled-components";

export const Main = styled.div`
  position: fixed; 
  top: 0; 
  left: 0; 
	
  /* Preserve aspet ratio */
  min-width: 100%;
  min-height: 100%;
  background: #FFAA15;
`;

export const Section = styled.div`
  height: 80vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const Container = styled.div``;

export const LandP = styled.p`
  letter-spacing: 4px;
  text-align: center;
  color: #203c38;
  font-size: 2.5rem;
  font-family: "Neucha", cursive;
  font-weight: bolder;
`;
export const LandH1 = styled.h1`
  text-align: center;
  font-family: "Cabin Sketch", cursive;
  font-size: 9rem;
  color: #fff;
`;
export const LandDesc = styled.h1`
  letter-spacing: 4px;
  text-align: center;
  color: #203c38;
  font-size: 3rem;
  font-family: "Neucha", cursive;
`;
export const LandButton = styled(Link)`
  letter-spacing: 2px;
  font-family: "Neucha", cursive;
  color: #fff;
  text-decoration: none;
  font-weight: bolder;
  font-size: 1.5rem;
  border-radius: 50%;
  border: 3px solid #fff;
  display: flex;
  justify-content: center;
  width: 15%;
  padding: 5px 0 5px 0;
  margin: 15px auto 10px auto;
  border-radius: 10px;
  cursor: pointer;
  ${"" /* Add hover effects */}
`;
export const IconWrapper = styled.div`
  text-align: center;
`;
export const Icon = styled.img`
  height: 30px;
  width: 30px;
  margin: 0 20px 0 20px;
  border-radius: 60px;
  filter: invert(100%);
  ${"" /* add hover effects */}
`;
