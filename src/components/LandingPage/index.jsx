import React from "react";
import Icon1 from "../../images/bullet-2.png";
import {
  Main,
  Section,
  Container,
  LandP,
  LandH1,
  LandDesc,
  Icon,
  IconWrapper,
  LandButton,
} from "./LandingElements";

const LandingPage = () => {
  return (
    <>
      <Main>
        <Section>
          <Container>
            <LandP>HEY, I'M</LandP>
            <LandH1>AREEJ GABRIELLE</LandH1>
            <LandDesc>WEB DEVELOPER</LandDesc>
            <LandButton>EXPLORE</LandButton>
          </Container>
        </Section>
        <IconWrapper>
          <Icon src={Icon1}></Icon>
          <Icon src={Icon1}></Icon>
          <Icon src={Icon1}></Icon>
          <Icon src={Icon1}></Icon>
        </IconWrapper>
      </Main>
      
    </>
  );
};

export default LandingPage;
