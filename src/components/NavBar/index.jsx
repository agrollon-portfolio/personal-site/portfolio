import React from "react";
import {
  Nav,
  NavbarContainer,
  MobileIcon,
  NavMenu,
  NavLinks,
  NavItem,
} from "./NavBarElements";

const NavBar = () => {
  return (
    <Nav>
      <NavbarContainer>
        <MobileIcon></MobileIcon>
        <NavMenu>
          <NavItem>
            <NavLinks>About</NavLinks>
          </NavItem>
          <NavItem>
            <NavLinks>Skills</NavLinks>
          </NavItem>
          <NavItem>
            <NavLinks>Projects</NavLinks>
          </NavItem>
          <NavItem>
            <NavLinks>Contact</NavLinks>
          </NavItem>
        </NavMenu>
      </NavbarContainer>
    </Nav>
  );
};

export default NavBar;
